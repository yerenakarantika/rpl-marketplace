<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$data['user'] = $this->db->get_where('user', ['email' =>

		$this->session->userdata('email')])->row_array();
		if ($data['user'] == NULL) {
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Login Terlebih dahulu
          </div>');
			redirect('auth');
		}
		if (($data['user']['role_id'] == 2) == false) {

			echo "that has been forbidden";
			redirect('admin/dashboard_admin');
		}
	}




	public function index()
	{

		$data['barang'] = $this->model_barang->tampil_data()->result();
		$data['user'] = $this->db->get_where('user', ['email' =>

		$this->session->userdata('email')])->row_array();

		$this->load->view('templates/header');
		$this->load->view('templates/sidebar', $data);
		$this->load->view('user/dashboard', $data);
		$this->load->view('templates/footer');
	}
}
