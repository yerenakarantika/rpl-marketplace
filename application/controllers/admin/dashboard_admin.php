<?php

class Dashboard_admin extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$data['user'] = $this->db->get_where('user', ['email' =>

		$this->session->userdata('email')])->row_array();


		if ($data['user'] == NULL) {
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Login Terlebih dahulu
          </div>');
			redirect('auth');
		}
		if (($data['user']['role_id'] == 1) == false) {

			redirect('user/User');
		}
	}
	public function index()
	{



		$data['user'] = $this->db->get_where('user', ['email' =>

		$this->session->userdata('email')])->row_array();


		$this->load->view('templates_admin/header');
		$this->load->view('templates_admin/sidebar', $data);
		$this->load->view('admin/dashboard', $data);
		$this->load->view('templates_admin/footer');
	}
}
